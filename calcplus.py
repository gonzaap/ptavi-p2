import sys
import calcoohija


if __name__ == "__main__":
    fichero = open(sys.argv[1])
    texto = fichero.readlines()
    for linea in texto:
        elementos = linea.split(",")
        operando = elementos[0]
        resultado = int(elementos[1])
        for dato in elementos[2:]:

            operador = int(dato)
            objeto = calcoohija.CalculadoraHija(resultado, operador)

            if operando == "suma":
                resultado = objeto.plus()
            elif operando == "resta":
                resultado = objeto.minus()
            elif operando == "multiplica":
                resultado = objeto.multi()
            elif operando == "divide":
                resultado = objeto.div()
            else:
                sys.exit("Por favor, elija ")

        print(resultado)
