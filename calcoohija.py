#!/usr/bin/python3

import sys
import calcoo


class CalculadoraHija(calcoo.Calculadora):

    def multi(self):

        return self.valor1 * self.valor2

    def div(self):
        try:
            return self.valor1 / self.valor2
        except ZeroDivisionError:
            sys.exit("Division by zero is not allowed")


if __name__ == "__main__":
    try:
        operando1 = int(sys.argv[1])
        operando2 = int(sys.argv[3])
    except ValueError:
        sys.exit("Error: Non numerical parameters")

    objeto = CalculadoraHija(operando1, operando2)

    if sys.argv[2] == "suma":
        result = objeto.plus()
    elif sys.argv[2] == "resta":
        result = objeto.minus()
    elif sys.argv[2] == "por":
        result = objeto.multi()
    elif sys.argv[2] == "dividido":
        result = objeto.div()
    else:
        sys.exit('Operación sólo puede sumar, restar, multiplicar o dividir')
    print(result)
