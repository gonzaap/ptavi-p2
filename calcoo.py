#!/usr/bin/python3
# -- coding: utf-8 --

import sys


class Calculadora():

    def __init__(self, operando1, operando2):
        self.valor1 = operando1
        self.valor2 = operando2

    def plus(self):

        return self.valor1 + self.valor2

    def minus(self):

        return self.valor1 - self.valor2


if __name__ == "__main__":
    try:
        operando1 = int(sys.argv[1])
        operando2 = int(sys.argv[3])
    except ValueError:
        sys.exit("Error: Non numerical parameters")

    objeto = Calculadora(operando1, operando2)
    if sys.argv[2] == "suma":
        result = objeto.plus()
    elif sys.argv[2] == "resta":
        result = objeto.minus()
    else:
        sys.exit('Operación sólo puede ser sumar o restar.')
    print(result)
