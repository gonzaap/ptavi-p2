import sys
import calcoohija
import csv


if __name__ == "__main__":
    with open(sys.argv[1], newline="") as fichero:
        texto = csv.reader(fichero)

        for linea in texto:
            operando = linea[0]
            resultado = int(linea[1])
            for dato in linea[2:]:

                operador = int(dato)
                objeto = calcoohija.CalculadoraHija(resultado, operador)

                if operando == "suma":
                    resultado = objeto.plus()
                elif operando == "resta":
                    resultado = objeto.minus()
                elif operando == "multiplica":
                    resultado = objeto.multi()
                elif operando == "divide":
                    resultado = objeto.div()
                else:
                    sys.exit("Por favor, elija ")

            print(resultado)
